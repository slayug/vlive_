#!/bin/bash
# Remove old files
rm vlive*
rm ca.crt ca.key ca.srl
rm wildcard.dev.*
# Generate CA CRT
openssl req -new -x509 -out ca.crt -days 36500 -config ca.conf
# Generate wildcard certificate
openssl req -new -out wildcard.dev.vlive.fr.csr -config wildcard.conf
openssl req -new -x509 -in wildcard.dev.vlive.fr.csr -out ca.crt -days 36500 -config ca.conf
openssl x509 -req -in wildcard.dev.vlive.fr.csr -CA ca.crt -CAkey ca.key -CAcreateserial -extfile wildcard.conf -extensions v3_req -days 36500 -sha256 -out wildcard.dev.vlive.fr.crt
# Generate certificate chain bundle for reverse proxy
cat wildcard.dev.vlive.fr.crt >> wildcard.dev.vlive.fr.bundle.crt
cat ca.crt >> wildcard.dev.vlive.fr.bundle.crt
# Generate p12 (for java keystore?)
openssl pkcs12 -export -name "Vlive Dev" -in ca.crt -inkey ca.key -passout pass:vlive -out vlive-dev.p12
# Generate java keystore
keytool -keystore vlive-truststore.jks -importcert -file 'ca.crt' -keypass vlive-storepass vlive-noprompt
