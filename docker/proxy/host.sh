#!/bin/sh

HOST_DOMAIN="host.docker.internal"

ping -q -c1 $HOST_DOMAIN > /dev/null 2>&1
if [ $? -ne 0 ]; then
  HOST_IP=$(ip route | awk 'NR==1 {print $3}')
  echo -e "\n Using Docker for UNIX"
else
  HOST_IP=`nslookup host.docker.internal | awk '/^Address/ { print $3 }'`
  echo -e "\n Using Docker for Mac or Windows"
fi
echo -e "$HOST_IP\t$HOST_DOMAIN" >> /etc/hosts

echo -e "$HOST_IP\t$HOST_DOMAIN"

# Run local DNS server based on /etc/hosts entries
dnsmasq

nginx -g "daemon off;"
