
import { readdir, lstatSync, renameSync } from 'fs';
import path from 'path';

const directory = './src/';

function iterate(dir) {
    readdir(dir, (err, files) => {
        files.forEach(file => {
        if (lstatSync(path.resolve(dir, file)).isDirectory()) {
            iterate(path.resolve(dir, file));
        } else {
          if (file.indexOf(".mjs") !== -1) {
            console.log('rename: ' + file);
            renameSync(path.resolve(dir, file), path.resolve(dir, file.replace("mjs", "ts")));

          }
        }
      });
        console.log(files);
    });
}

iterate(directory);