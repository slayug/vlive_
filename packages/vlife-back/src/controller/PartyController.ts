import { getCurrentSessionPeon } from '../util/session';
import PartyDao from '../dao/PartyDao';

import Express, { Request } from 'express';
const router = Express.Router();

router.get('/:id', (request, res) => {
  const party = PartyDao.get(request.params.id);
  if (party === undefined) {
    return res.status(404).send();
  }

  return res.status(200).send(party);
});

router.post('', (request: Request, res) => {
  const peon = getCurrentSessionPeon(request);

  if (request.body.url === undefined) {
    return res.status(400).send();
  }

  const party = PartyDao.createParty(request.body, peon);

  return res.status(200).send(party);
});

export default router;
