import { getCurrentSessionPeon } from '../util/session';

import Express from 'express';
const router = Express.Router();

router.get('/me', (request, res) => {
  return res.status(200).send(getCurrentSessionPeon(request));
});

export default router;