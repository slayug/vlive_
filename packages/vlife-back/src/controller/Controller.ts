import Express, { Application } from 'express';
const router = Express.Router();
export default class Controller {
  constructor(private mainPath: string) {}

  route() {
    return router;
  }
}
