export default class Answer {
    /**
     *
     * @param success boolean to say if everything went well
     * @param reason message to say what happened
     */
    constructor(private success: boolean, reason: string) {
        this.success = success;
        this.reason = reason;
    }

    ifFailed(task) {
        if (!this.success)
            task();
        return this;
    }

    ifSucceeded(task) {
        if (this.success)
            task();
        return this;
    }

    anyway(task) {
        task();
        return this;
    }
}