import peonData from '../dao/PeonDao';

export function getCurrentSessionPeon(request) {

    if (!request.session.peon) {
        console.log('create PEON');
        request.session.peon = peonData.createPeon();
    }

        console.log(request.session.peon);
    return request.session.peon;
}