import { customAlphabet, nanoid } from 'nanoid';

/**
 * The maximum is exclusive and the minimum is inclusive
 */
export function getRandomIntInclusive(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Generate a id with full symbols (A-Za-z0-9_-)
 * @param size Default to 8
 * @returns
 */
export function generateID(size = 8) {
  return nanoid(size);
}

const hexNanoid = customAlphabet('ABCDEF0123456789', 6);
export function generateHexColor() {
  return `#${hexNanoid()}`;
}

const COLOR_LIST = [
  '#fc5c65',
  '#eb3b5a',
  '#fd9644',
  '#fa8231',
  '#fed330',
  '#f7b731',
  '#26de81',
  '#20bf6b',
  '#2bcbba',
  '#0fb9b1',
  '#45aaf2',
  '#2d98da',
  '#4b7bec',
  '#3867d6',
  '#a55eea',
  '#8854d0',
  '#d1d8e0',
  '#a5b1c2',
  '#778ca3',
  '#4b6584',
];

export function getRandomColorList() {
  return shuffleArray(COLOR_LIST);
}

// https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
export function shuffleArray(a: any[]) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}
