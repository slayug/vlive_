import cors from 'cors';
import express, { Application } from 'express';
import expressSession from 'express-session';
import { Server as SocketIOServer, Socket } from 'socket.io';
import sharedsession from 'express-socket.io-session';
import { createServer, Server as HTTPServer } from 'http';
import PartyController from './controller/PartyController';

import { v4 as uuid } from 'uuid';
import PeonController from './controller/PeonController';
import { setupSocketIo } from './socketService';
import { PacketPartyJoin, PacketPartyLeft } from '@vlive/common/packet/Packet';

const PORT = '8000';
const HOST = '0.0.0.0';

export class Server {
  private httpServer: HTTPServer;
  private app: Application;
  private io: SocketIOServer;

  constructor() {
    this.app = express();
    this.app.use(
      cors({ credentials: true, origin: 'https://www.dev.vlive.fr' })
    );
    this.app.use(express.json());

    this.httpServer = createServer(this.app);
    this.io = new SocketIOServer(this.httpServer);

    this.setupSession();
    this.setupRoutes();
    setupSocketIo(this.io);
  }

  setupSession() {
    // If you have your node.js behind a proxy and are using secure: true,
    // you need to set "trust proxy" in express:
    this.app.set('trust proxy', 1); // trust first proxy
    const session = expressSession({
      genid: function (req) {
        return uuid(); // use UUIDs for session IDs
      },
      secret: 'fnezfnNF89E8F9nf00bEfezMLSA',
      resave: true,
      saveUninitialized: true,
      cookie: { secure: true, maxAge: 600000 },
    });
    this.app.use(session);
    this.io.use(sharedsession(session, { autoSave: true }));
  }

  setupRoutes() {
    console.log('Setup routes..');
    this.app.use('/party', PartyController);
    this.app.use('/peon', PeonController);
  }

  public listen(callback: (port: number) => void): void {
    const port = parseInt(PORT) || 3030;

    this.httpServer.listen(port, () => {
      callback(port);
    });
  }
}

// Start the server
const init = async () => {
  console.log('Starting server..');
  try {
    const server = new Server();

    server.listen((port) => {
      console.log(`Server is listening on https://localhost:${port}`);
    });
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

init();
