import NodeCache from 'node-cache';
import { Peon } from '@vlive/common';
import Phil from '../phil';
import { generateHexColor, generateID, getRandomIntInclusive } from '../util/random';

// Seven days 3600*24*7
const SAVE_TIME = 604800;

class PeonDao {
    cache: any;

    constructor() {
        // stdTTL: (default: 0) the standard ttl as number in seconds for every generated cache element. 0 = unlimited
        // checkperiod: (default: 600) The period in seconds, as a number, used for the automatic delete check interval. 0 = no periodic check.
        this.cache = new NodeCache({ stdTTL: 3600, checkperiod: 60 });
    }

    get(id: string): Peon {
        return this.cache.get(id);
    }

    findBySocketId(socketId: string): Peon | undefined {
        for (let key of this.cache.keys()) {
            const peon = this.cache.get(key);
            if (peon.socketId === socketId) {
                return peon;
            }
        }
        return undefined;
    }

    /**
     * @return Answer object
     * @param party contains url: videoUrl
     */
    createPeon(color?: string) {
        let peon = undefined;
        if (color) {
            // generate one
            peon = PeonDao.generate(color)
        } else {
            peon = PeonDao.generate(generateHexColor());
        }
        this.cache.set(peon.id, peon, SAVE_TIME);

        return peon;
    }

    savePeon(peon: Peon) {
        if (!peon.id) {
            throw Error('Peon must got id to be saved');
        }
        this.cache.set(peon.id, peon, SAVE_TIME)
    }

    deleteParty(party) {
        this.cache.del(party.id);
    }


    static generate(color) {
        const pokemons = new Phil().pokemon();
        const index = getRandomIntInclusive(0, pokemons.length);
        return new Peon(
            generateID(12),
            pokemons[index],
            color
        );
    }

}

const peonData = new PeonDao();
export default peonData;