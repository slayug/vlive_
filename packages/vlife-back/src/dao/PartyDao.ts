import NodeCache from 'node-cache';
import Party from '@vlive/common/entity/Party';
import { generateID } from '../util/random';

class PartyDao {
    cache: any;

    constructor() {
        // stdTTL: (default: 0) the standard ttl as number in seconds for every generated cache element. 0 = unlimited
        // checkperiod: (default: 600) The period in seconds, as a number, used for the automatic delete check interval. 0 = no periodic check.
        this.cache = new NodeCache({ stdTTL: 3600, checkperiod: 60 });
    }

    get(id) {
        // instance first the party
        const partyCache = this.cache.get(id);
        if (partyCache) {
            return Party.from(this.cache.get(id));
        } else {
            console.error('Party not found', id);
            return undefined;
        }
    }

    /**
     * @return Answer object
     * @param party contains url: videoUrl
     */
    createParty(party, peon) {
        if (party.url === undefined) {
            throw 'Party malformed, should contains { url: videoUrl }';
        }

        const realParty = new Party('toast', party.url, peon);
        realParty.id = generateID();
        peon.color = realParty.getCurrentColor();

        //TODO also set time of the video plus smth like 10min as TTL
        this.save(realParty);

        return realParty;
    }

    save(party) {
        this.cache.set(party.id, party, 3600);
    }

    deleteParty(party) {
        this.cache.del(party.id);
    }

}

const partyDao = new PartyDao();
export default partyDao;
