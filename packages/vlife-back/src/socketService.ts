import { Peon } from '@vlive/common';
import {
  PacketPartyJoin,
  PacketPartyLeft,
  PacketPeonPartyList,
} from '@vlive/common/packet/Packet';
import { Server as SocketIOServer, Socket } from 'socket.io';
import peonData from './dao/PeonDao';

export function setupSocketIo(io: SocketIOServer) {

  io.on('connect', (socket: Socket) => {
    try {
      const peonId: string = (socket.handshake as any).session.peon.id;
      const peon = peonData.get(peonId);
      peon.socketId = socket.id;
      peonData.savePeon(peon);
    } catch (e) {
      console.error('Cannot identify socket ' + socket.id, e);
      socket.disconnect();
    }

    socket.use((packet, next) => {
      // Parse each package
      if (packet[0] !== 'signal') {
        try {
          packet[1] = JSON.parse(packet[1]);
        } catch (e) {
          console.error('Cannot parse', packet);
        }
      }
      try {
        next();
      } catch (e) {
        console.error('Something bad for ' + socket.id);
        console.error(e);
      }
    });

    socket.on(PacketPartyJoin.ENDPOINT, (packet: PacketPartyJoin) => {
      packet.who = (socket.handshake as any).session.peon;
      packet.socketId = socket.id;

      // notify others
      socket.to(packet.partyId).emit(packet.endpoint, JSON.stringify(packet));

      // send others to new one
      const others = io.of('/').adapter.rooms.get(packet.partyId) ?? [];
      const peonsConnected = Array.from(others).map((oSocket) => {
        return peonData.findBySocketId(oSocket);
      });
      socket.emit(
        PacketPeonPartyList.ENDPOINT,
        JSON.stringify(new PacketPeonPartyList(packet.partyId, peonsConnected))
      );

      // join room
      socket.join(packet.partyId);
    });

    socket.on(PacketPartyLeft.ENDPOINT, (packet: PacketPartyLeft) => {
      socket.leave(packet.partyId);

      packet.who = (socket.handshake as any).session.peon;
      socket.to(packet.partyId).emit(packet.endpoint, JSON.stringify(packet));
    });

    /**
     * relay a peerconnection signal to a specific socket
     */
    socket.on('signal', (data: { socketId: string; signal: string }) => {
      io.to(data.socketId).emit('signal', {
        socketId: socket.id,
        signal: data.signal,
      });
    });
  });
}
