import { readFileSync } from 'fs';
import yaml from 'js-yaml';
/**
 * Phil (Fill) a random name, depends of the method call.
 */

const RESOURCES_PATH = './src/resources/';

export default class Phil {
    lng: string;
    constructor(lng?: string) {
        if (lng !== undefined) {
            this.lng = lng;
        } else {
            this.lng = 'en';
        }
    }

    pokemon() {
        try {
            const file = yaml.safeLoad(readFileSync(RESOURCES_PATH + this.lng + '.yml', 'utf8'));
            return file.pokemon.names;
        } catch (e) {
            console.error(e);
        }
    }

    got() {

    }

    star_trek() {

    }

    lebowski() {

    }

    hobbit() {}



}