import { Peon } from "../entity";

export default abstract class Packet {
  public who?: Peon;
  public partyId?: string;

  constructor(public endpoint: string) {}

  toString() {
    return JSON.stringify(this);
  }
}

export class PacketPartyLeft extends Packet {
  public static ENDPOINT = "left";

  constructor(public id: string, public who?: Peon) {
    super(PacketPartyLeft.ENDPOINT);
  }
}

export class PacketPartyJoin extends Packet {
  public static ENDPOINT = "join";

  constructor(
    public partyId: string,
    public who?: Peon,
    public socketId?: string
  ) {
    super(PacketPartyJoin.ENDPOINT);
  }
}

export class PacketPeonPartyList extends Packet {
  public static ENDPOINT = "peonPartyList";

  constructor(public partyId: string, public peons: Peon[]) {
    super(PacketPeonPartyList.ENDPOINT);
  }
}

export class PacketDataJoin extends Packet {
  public static ENDPOINT = "dataJoin";

  constructor(
    public toId: number,
    public fromSocketId?: string,
    public fromId?: number
  ) {
    super(PacketDataJoin.ENDPOINT);
  }
}
