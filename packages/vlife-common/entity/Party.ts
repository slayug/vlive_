import { getRandomColorList } from "../../vlife-back/src/util/random";
import Peon from "./Peon";

export default class Party {
  id: string;
  peons: Peon[];
  anarchy: boolean;
  colorIndex: number;

  constructor(public name: string, public url: string, public mainPeon: Peon) {
    this.id = "";
    this.peons = [];
    this.anarchy = false;
    this.colorIndex = 0;
  }

  static from(obj) {
    // TODO check each field is valid
    const party = new Party(obj.name, obj.url, Peon.from(obj.mainPeon));
    party.id = obj.id;
    party.peons = obj.peons;
    party.anarchy = obj.anarchy;
    party.colorIndex = obj.colorIndex;

    return party;
  }

  getCurrentColor() {
    return this.colorIndex[this.colorIndex];
  }

  containsPeon(peon: Peon) {
    for (let p of this.peons) {
      if (peon.id === p.id) {
        return true;
      }
    }
    return this.mainPeon.id === peon.id;
  }

  addPeon(peon: Peon) {
    if (!this.containsPeon(peon)) {
      this.peons.push(peon);
    }
  }
}
