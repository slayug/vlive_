export default class MetaPlayer {
  public id: number;
  public colors: { r: number; g: number; b: number };
  public position? = { x: 0, y: 0, z: 0 };
  public inputs?: [];

  constructor() {
    this.inputs = [];
    this.colors = { r: 255, g: 255, b: 255 };
  }
}
