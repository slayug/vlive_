import { Vector3 } from "babylonjs";

export interface Position {
    x: number;
    y: number;
    z: number;
}

export function vector3ToPosition(vector: Vector3) {
    const { x, y, z } = vector;
    return {x, y, z};
}
export function positionToVector3(position: Position) {
    return new Vector3(position.x, position.y, position.z);
}