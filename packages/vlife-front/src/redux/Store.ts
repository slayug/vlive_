import {
  combineReducers,
  configureStore,
  getDefaultMiddleware,
} from "@reduxjs/toolkit";
import { createLogger } from "redux-logger";

import peonSlice from "./slices/PeonSlice";

import { useDispatch } from "react-redux";

const reducer = combineReducers({
  peonSlice,
});

const logger = createLogger({ collapsed: true });

const middleware = getDefaultMiddleware().concat(logger);

const store = configureStore({
  reducer: reducer,
  middleware,
});

export type RootState = ReturnType<typeof reducer>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export default store;
