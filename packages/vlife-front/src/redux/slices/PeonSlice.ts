import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Peon } from "@vlive/common/entity";

export type PeonSliceType = {
  currentPeon?: Peon;
};

const initialState: PeonSliceType = {
  currentPeon: undefined,
};

export const peonSlice = createSlice({
  name: "peon",
  initialState,
  reducers: {
    setCurrentPeon: (state, action: PayloadAction<Peon>) => {
      state.currentPeon = action.payload;
    },
  },
});

export const {
  setCurrentPeon
} = peonSlice.actions;

export default peonSlice.reducer;
