import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/home/HomePage";
import Party from "./pages/party/PartyPage";
import {
  PartyPageContextProvider,
} from "./pages/party/PartyPageContext";

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/party/:id">
        <PartyPageContextProvider>
          <Party />
        </PartyPageContextProvider>
      </Route>
    </Switch>
  );
}
