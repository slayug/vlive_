declare const Config: {
    LOG_LEVEL: log.LogLevelDesc,
    API_HOST: string,
    STUN_SERVER: string,
}