
export default interface Party {
    name?: string,
    id?: string,
    url: string,
}