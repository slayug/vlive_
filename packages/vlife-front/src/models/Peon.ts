export default class Peon {

    public socketId?: string;

    constructor(public id: string, public name: string, public color: string) {}

    static from(obj: any) {
        //TODO check each field
        return new Peon(obj._id, obj.name, obj.color);
    }

}
