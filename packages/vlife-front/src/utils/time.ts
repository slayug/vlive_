
/**
 * 
 * @param {second} time 
 * @param {ms} fromDate 
 * @returns time in second
 * 
 */
 export function shiftTimeToLive(time, fromDate) {
    return time + (Date.now() - fromDate) / 1000;
}