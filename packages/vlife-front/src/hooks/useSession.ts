import { useSelector } from "react-redux"
import { PeonSliceType } from "../redux/slices/PeonSlice";
import { RootState } from "../redux/Store";


export default function useSession() {

    const peonStore: PeonSliceType = useSelector<RootState>(state => state.peonSlice);

    function getCurrentPeon() {
        if(!peonStore.currentPeon) {
            throw Error("No peon identified");
        }
        return peonStore.currentPeon;
    }

    return {
        getCurrentPeon
    }
}