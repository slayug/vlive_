import { Layout } from "antd";
import { Header, Content, Footer } from "antd/lib/layout/layout";
import { BrowserRouter as Router } from "react-router-dom";

import log from 'loglevel';
import prefix from 'loglevel-plugin-prefix';

import Routes from "./Routes";
import OwnHeader from "./components/header/Header";

import store from "./redux/Store";
import { Provider } from "react-redux";

import { QueryClient, QueryClientProvider } from "react-query";

import "./App.less";
import SessionComponent from "./components/session/SessionComponent";

const logger = log.noConflict();
prefix.reg(logger);
prefix.apply(logger);
log.setLevel(Config.LOG_LEVEL);

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <QueryClientProvider client={queryClient}>
          <Router>
            <Layout>
              <Header>
                <OwnHeader />
              </Header>
              <Content>
                <SessionComponent>
                  <Routes />
                </SessionComponent>
              </Content>
              <Footer>Footer</Footer>
            </Layout>
          </Router>
        </QueryClientProvider>
      </div>
    </Provider>
  );
}

export default App;
