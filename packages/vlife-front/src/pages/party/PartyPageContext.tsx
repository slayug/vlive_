import React, { useEffect, useRef, useState } from "react";
import { YouTubePlayer } from "youtube-player/dist/types";
import Party from "../../models/Party";
import Packet, {
  PacketPartyJoin,
  PacketPartyLeft,
  PacketPauseVideo,
  PacketPeonPartyList,
  PacketPlayVideo,
} from "../../packets/Packet";
import WebSocketManager from "../../services/WebSocketManager";
import WebRTCManager from "../../services/WebRTCManager";

export type PartyPlayPauseObserver = (
  time: number,
  currentTime: number
) => void;
export type onPartyPlayPause = (observer: PartyPlayPauseObserver) => void;

export type PartyPageContextProps = {
  youtubePlayer?: YouTubePlayer;
  currentParty?: Party;
  /**
   * True when the init process of joining the party is done.
   */
  joined: boolean;
  play: (time: number) => void;
  pause: (time: number) => void;
  onPlay: onPartyPlayPause;
  onPause: onPartyPlayPause;
  joinParty: (party: Party) => void;
  exitParty: () => void;
  listenPacket<T extends Packet>(
    endpoint: string,
    callback: (packet: T) => void
  ): void;
};

export const PartyPageContext = React.createContext<
  Partial<PartyPageContextProps>
>({});

export function PartyPageContextProvider({
  children,
}: {
  children: JSX.Element;
}) {
  const [party, setParty] = useState<Party>();
  const [joined, setJoined] = useState(false);
  const [ready, setReady] = useState(false);

  const webSocketRTC = useRef<WebRTCManager>();
  const webSocketManager = useRef<WebSocketManager>();
  const playListeners = useRef<PartyPlayPauseObserver[]>([]);
  const pauseListeners = useRef<PartyPlayPauseObserver[]>([]);

  useEffect(() => {
    webSocketManager.current = new WebSocketManager();
    webSocketManager.current.connect().then(() => {
      webSocketRTC.current = new WebRTCManager(webSocketManager.current);
      setReady(true);

      getWebSocketRTC().on<PacketPlayVideo>(
        PacketPlayVideo.ENDPOINT,
        (from: string, packet: PacketPlayVideo) => {
          playListeners.current.forEach((listener) => listener(packet.time, packet.currentTime));
        }
      );
      getWebSocketRTC().on<PacketPauseVideo>(
        PacketPauseVideo.ENDPOINT,
        (from: string, packet: PacketPlayVideo) => {
          pauseListeners.current.forEach((listener) => listener(packet.time, packet.currentTime));
        }
      );

      getWebSocketManager().on(
        PacketPeonPartyList.ENDPOINT,
        (packet: PacketPeonPartyList) => {
          Array.from(packet.peons).forEach((peon) => {
            getWebSocketRTC().addPeer(peon.socketId, false);
          });
        }
      );
    });
  }, []);

  function getWebSocketManager() {
    return webSocketManager.current;
  }

  function getWebSocketRTC() {
    return webSocketRTC.current;
  }

  useEffect(() => {
    return () => {
      if (party) {
        webSocketManager.current.sendPacket(new PacketPartyLeft(party.id));
      }
    };
  }, [party]);

  function joinParty(party: Party) {
    setParty(party);
    webSocketManager.current.sendPacket(new PacketPartyJoin(party.id));
    setJoined(true);
  }

  function listenPacket<T extends Packet>(
    endpoint: string,
    callback: (packet: T) => void
  ) {
    if (webSocketManager.current) {
      webSocketManager.current.on<T>(endpoint, callback);
    }
  }

  function exitParty() {
    playListeners.current = [];
    pauseListeners.current = [];
  }

  function play(time: number) {
    getWebSocketRTC().broadcast(new PacketPlayVideo(time));
  }

  function pause(time: number) {
    getWebSocketRTC().broadcast(new PacketPauseVideo(time));
  }

  function onPause(observer: PartyPlayPauseObserver) {
    pauseListeners.current.push(observer);
  }

  function onPlay(observer: PartyPlayPauseObserver) {
    playListeners.current.push(observer);
  }

  return (
    <PartyPageContext.Provider
      value={{
        joined,
        currentParty: party,
        joinParty,
        exitParty,
        onPause,
        onPlay,
        play,
        pause,
        listenPacket,
      }}
    >
      {ready && children}
    </PartyPageContext.Provider>
  );
}
