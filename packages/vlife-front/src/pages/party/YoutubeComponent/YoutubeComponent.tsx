import { useContext, useEffect, useState } from "react";
import YouTube from "react-youtube";
import { YouTubePlayer } from "youtube-player/dist/types";
import { PartyPageContext } from "../PartyPageContext";
import useParty from "../useParty";

import log from "loglevel";

import styles from "./YoutubeComponent.module.scss";
import { shiftTimeToLive } from "../../../utils/time";

export default function YoutubeComponent() {
  const { pause, play, currentParty, onPlay, onPause } =
    useContext(PartyPageContext);
  const [youtubePlayer, setYoutubePlayer] = useState<YouTubePlayer>(undefined);

  useParty({
    ready: youtubePlayer !== undefined,
  });

  useEffect(() => {
    if (onPlay && youtubePlayer) {
      log.debug('onPlay & youtubePlayer is ready');
      onPlay((time, currentTime) => {
        youtubePlayer.playVideo();
        youtubePlayer.playVideoAt(shiftTimeToLive(time, currentTime));
      });
    }
  }, [youtubePlayer, onPlay]);

  useEffect(() => {
    if (onPause && youtubePlayer) {
      onPause((time, currentTime) => {
        youtubePlayer.pauseVideo();
        youtubePlayer.seekTo(shiftTimeToLive(time, currentTime), true);
      });
    }
  }, [onPause, youtubePlayer]);

  return (
    <div className={styles.youtube}>
      <YouTube
        opts={{
          width: "100%",
          height: "100%",
        }}
        videoId={currentParty?.url.split("=")[1]}
        onPause={(event) => pause(event.target.getCurrentTime())}
        onPlay={(event) => play(event.target.getCurrentTime())}
        onReady={(event) => setYoutubePlayer(event.target)}
      />
    </div>
  );
}
