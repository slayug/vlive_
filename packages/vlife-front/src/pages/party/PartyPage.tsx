import { useContext, useEffect } from "react";
import { Alert, Spin } from "antd";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { partyService } from "../../services/Service";

import styles from "./PartyPage.module.scss";
import { PartyPageContext } from "./PartyPageContext";
import YoutubeComponent from "./YoutubeComponent/YoutubeComponent";
import ChatComponent from "./ChatComponent/ChatComponent";

export default function PartyPageComponent() {
  let { id } = useParams() as { id: string | undefined };
  const { joined, joinParty, exitParty } = useContext(PartyPageContext);

  const {
    data: party,
    isSuccess,
    isLoading,
    isError,
  } = useQuery(`party-${id}`, () => partyService.get(id), {
    retry: false,
  });

  useEffect(() => {
    return () => {
      exitParty();
    };
  }, []);

  useEffect(() => {
    if (isSuccess && !joined && joinParty) {
      joinParty(party);
    }
  }, [isSuccess, joined, joinParty, party]);

  return (
    <div className={styles.party}>
      {isError ? (
        <Alert type="error" message="Cannot load the current party" />
      ) : isLoading ? (
        <Spin />
      ) : (
        <div className={styles.content}>
          <div className={styles.video}>
            <YoutubeComponent />
          </div>
          <div className={styles.chat}>
            <ChatComponent />
          </div>
        </div>
      )}
    </div>
  );
}
