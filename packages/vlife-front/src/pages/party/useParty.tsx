import { useContext, useEffect } from "react";
import { PartyPageContext, PartyPlayPauseObserver } from "./PartyPageContext";

export default function useParty(events: {
  onPlay?: PartyPlayPauseObserver;
  onPause?: PartyPlayPauseObserver;
  ready: boolean;
}) {
  const { onPlay: onPlayContext, onPause: onPauseContext } =
    useContext(PartyPageContext);

  useEffect(() => {
    if (onPlayContext && events.onPlay) {
      onPlayContext(events.onPlay);
    }
  }, [onPlayContext, events.onPlay]);

  useEffect(() => {
    if (onPauseContext && events.onPause) {
      onPauseContext(events.onPause);
    }
  }, [onPauseContext, events.onPause]);
}
