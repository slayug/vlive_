import { useContext, useEffect, useState } from "react";
import styles from "./ChatComponent.module.scss";

import { v4 as uuid } from "uuid";
import { PartyPageContext } from "../PartyPageContext";
import { PacketPartyJoin, PacketPartyLeft, PacketPeonPartyList } from "../../../packets/Packet";
import useSession from "../../../hooks/useSession";
import log from "loglevel";

interface Message {
  type: "message" | "event";
  content: string;
  id?: string;
}

export default function ChatComponent() {
  const { listenPacket } = useContext(PartyPageContext);
  const [messages, setMessages] = useState<Message[]>([]);

  const { getCurrentPeon } = useSession();

  function appendMessage(message: Message) {
    setMessages((prevMessages) => [
      ...prevMessages,
      { id: uuid(), ...message },
    ]);
  }

  useEffect(() => {
    listenPacket(PacketPartyJoin.ENDPOINT, (packet: PacketPartyJoin) => {
      appendMessage({
        type: "event",
        content: `${packet.who.name} connected.`,
      });
    });
    listenPacket(PacketPartyLeft.ENDPOINT, (packet: PacketPartyLeft) => {
      appendMessage({
        type: "event",
        content: `${packet.who.name} disconnected.`,
      });
    });
    listenPacket(PacketPeonPartyList.ENDPOINT, (packet: PacketPeonPartyList) => {
      packet.peons.forEach((peon) => {
        appendMessage({
          type: "event",
          content: `${peon.name} already in.`
        })
      });
    })
  }, []);

  return (
    <div className={styles.chat}>
      <div className={styles.messages}>
        {messages.map((message) => {
          return <div key={message.id}>{message.content}</div>;
        })}
      </div>
      <div className={styles.bottom}>
        <div className={styles.name} style={{ backgroundColor: getCurrentPeon().color ?? '#77FF77' }}>
          {getCurrentPeon().name.toUpperCase()}:
        </div>{" "}
        <input></input>
      </div>
    </div>
  );
}
