import styles from "./HomePage.module.scss";
import { Button, Col, Input, Row, Form, Alert } from "antd";
import { useHistory } from "react-router-dom";
import { partyService } from "../../services/Service";
import Party from "../../models/Party";
import { useMutation } from "react-query";

export default function Home() {
  const history = useHistory();
  const [form] = Form.useForm();
  const youtubeRegex = /^https?:\/\/(w{3}\.)?youtube.com\/watch\?v=(.){4,}/;

  const { mutate, isError, isLoading } = useMutation(
    (party: Party) => partyService.post(party),
    {
      onSuccess: (party) => {
        history.push(`/party/${party.id}`);
      },
    }
  );

  function createParty(values: { url: string }) {
    mutate(values);
  }

  return (
    <div className={styles.home}>
      <Row justify="center">
        <Col xs={24} sm={16} md={12} lg={20} xl={14}>
          {isError && <Alert message="Cannot create your party" type="error" />}
          <Form
            initialValues={{
              url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
            }}
            onFinish={createParty}
            form={form}
          >
            <Form.Item
              name="url"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Add a correct video url (Youtube, ?)",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (value.trim().match(youtubeRegex)) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error("Url is not valid"));
                  },
                }),
              ]}
            >
              <Input placeholder="https://www.youtube.com/watch?v=dQw4w9WgXcQ" />
            </Form.Item>
            <Form.Item>
              <Button loading={isLoading} htmlType="submit" type="primary">
                Create your party !
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
}
