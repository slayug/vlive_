import { NavLink } from "react-router-dom";
import styles from "./Header.module.scss";

export default function Header() {
  return (
    <div className={styles.header}>
      <NavLink to="/" exact>
          VLIVE
      </NavLink>
    </div>
  );
}
