import { Alert, Spin } from "antd";
import React, { useEffect } from "react";
import { useQuery } from "react-query";
import { setCurrentPeon } from "../../redux/slices/PeonSlice";
import { useAppDispatch } from "../../redux/Store";
import { peonService } from "../../services/Service";

export default function SessionComponent({ children }) {
  const dispatch = useAppDispatch();
  const {
    data: currentPeon,
    isLoading,
    isError,
  } = useQuery(`peon-me`, () => peonService.get("me"));

  useEffect(() => {
    if (currentPeon) {
      dispatch(setCurrentPeon(currentPeon));
    }
  }, [currentPeon, dispatch]);

  function center(children: React.ReactNode) {
    return (
      <div
        style={{ display: "flex", justifyContent: "center", marginTop: "10px" }}
      >
        {children}
      </div>
    );
  }

  return (
    <>
      {isLoading
        ? center(<Spin />)
        : isError
        ? center(<Alert message="Impossible de vous identifiez" type="error" />)
        : children}
    </>
  );
}
