import Peon from "../models/Peon";

export default abstract class Packet {
  public currentTime: number;
  constructor(public endpoint: string) {
    /**
     * Using basic clock time for each one,
     * later use IEEE 1588 spec
     * Init clock time when peer join one by one the party
     */
    this.currentTime = Date.now();
  }

  toString() {
    return JSON.stringify(this);
  }
}

export class PacketPlayVideo extends Packet {
  public static ENDPOINT = "packetPlayVideo";
  constructor(public time: number) {
    super(PacketPlayVideo.ENDPOINT);
  }
}
export class PacketPauseVideo extends Packet {
  public static ENDPOINT = "packetPauseVideo";
  constructor(public time: number) {
    super(PacketPauseVideo.ENDPOINT);
  }
}
export class PacketPartyJoinWaiting extends Packet {
  public static ENDPOINT = "partyJoinWaiting";

  constructor(public socketId: string) {
    super(PacketPartyJoinWaiting.ENDPOINT);
  }
}

export class PacketPartyJoin extends Packet {
  public static ENDPOINT = "join";

  constructor(
    public partyId: string,
    public who?: Peon,
    public socketId?: string
  ) {
    super(PacketPartyJoin.ENDPOINT);
  }
}
export class PacketPartyLeft extends Packet {
  public static ENDPOINT = "left";

  constructor(
    public partyId: string,
    public who?: Peon
  ){
    super(PacketPartyLeft.ENDPOINT);
  }
}

export class PacketDataJoin extends Packet {
  public static ENDPOINT = "dataJoin";

  constructor(
    public toId: number,
    public fromSocketId?: string,
    public fromId?: number
  ) {
    super(PacketDataJoin.ENDPOINT);
  }
}

export class PacketPeonPartyList extends Packet {
  public static ENDPOINT = "peonPartyList";

  constructor(public partyId: string, public peons: Peon[]) {
    super(PacketPeonPartyList.ENDPOINT);
  }
}