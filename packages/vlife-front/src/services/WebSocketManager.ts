import { io, Socket } from "socket.io-client";
import Packet from "@vlive/common/packet/Packet";
import log from "loglevel";

export default class WebSocketManager {
  private _socket: Socket;
  private inConnection = false;
  private packetsToSend: Packet[] = [];

  connect(): Promise<void> {
    return new Promise((resolve, _reject) => {
      this.inConnection = true;
      this._socket = io(`wss://${Config.API_HOST}`, {
        withCredentials: true,
        reconnectionDelayMax: 10000,
        transports: ["websocket", "polling", "flashsocket"],
      });
      this._socket.connect();
      this._socket.on("connect", () => {
        // send packet in waiting list
        this.inConnection = false;
        this.packetsToSend.forEach((packet) => {
          this.sendPacket(packet);
        });
        this.packetsToSend = [];
        resolve();
      });
    });
  }

  isConnected() {
    return this._socket.connected;
  }

  sendPacket(packet: Packet) {
    if (!this._socket.connected && !this.inConnection) {
      // init connection
      this.connect();
      this.packetsToSend.push(packet);
    } else if (!this._socket.connected) {
      // keep packet to send when connect
      this.packetsToSend.push(packet);
    } else {
      this._socket.emit(packet.endpoint, JSON.stringify(packet));
    }
  }

  on<T extends Packet>(endpoint: string, callback: (packet: T) => void) {
    this._socket.on(endpoint, (data) => {
      if (data) {
        const packet = JSON.parse(data);
        log.debug('packet receive', packet);
        callback(packet);
      }
    });
  }

  get socket() {
    return this._socket;
  }
}
