import { Peon } from "@vlive/common/entity";
import log from "loglevel";
import SimplePeer from "simple-peer";
import Packet, {
  PacketPartyJoin,
  PacketPartyJoinWaiting,
} from "../packets/Packet";
import WebSocketManager from "./WebSocketManager";

const configuration = {
  iceServers: [
    {
      urls: Config.STUN_SERVER,
    },
  ],
};
class DataPeer {
  constructor(private _peer: SimplePeer.Instance) {}

  get peer() {
    return this._peer;
  }
}

export default class WebRTCManager {
  private peonPeers: Map<string, DataPeer>;
  private waitingIdToConnect: Record<string, Peon>;
  private intervalWaitingPeon: NodeJS.Timeout;

  private dataListeners: Record<
    string,
    ((from: string, packet: any) => void)[]
  >;

  constructor(private wsManager: WebSocketManager) {
    this.dataListeners = {};
    this.peonPeers = new Map();
    this.waitingIdToConnect = {};
    this.initSocket();
  }

  peerAlreadyInitiated(socketId: string) {
    return this.peonPeers.has(socketId);
  }

  waitFor(socketId: string, peon: Peon) {
    if (!(socketId in this.waitingIdToConnect)) {
      this.waitingIdToConnect[socketId] = peon;
    }
  }

  removeOfWaiting(socketId: string) {
    delete this.waitingIdToConnect[socketId];
  }

  /**
   * Send to other connected peers
   */
  public broadcast(packet: Packet) {
    this.peonPeers.forEach((peonPeer) => {
      peonPeer.peer.send(JSON.stringify(packet));
    });
  }

  private initSocket() {
    // run setIntervalTo update waiting peon if necessary
    //TODO clear interval at some time
    this.intervalWaitingPeon = setInterval(() => {
      for (let socketId in this.waitingIdToConnect) {
        this.wsManager.sendPacket(new PacketPartyJoinWaiting(socketId));
      }
    }, 500);

    this.wsManager.on(PacketPartyJoin.ENDPOINT, (packet: PacketPartyJoin) => {
      if (!(packet.socketId in this.waitingIdToConnect)) {
        if (!this.peerAlreadyInitiated(packet.socketId)) {
          this.addPeer(packet.socketId, true);
          this.waitFor(packet.socketId, packet.who);
        }
      }
    });

    this.wsManager.on(
      PacketPartyJoinWaiting.ENDPOINT,
      (packet: PacketPartyJoinWaiting) => {
        this.addPeer(packet.socketId, false);
      }
    );

    this.wsManager.socket.on("removePeer", ({ socketId, _playerId }) => {
      this.removePeer(socketId);
    });

    this.wsManager.socket.on("disconnect", () => {
      //TODO handle disconnected case
      for (let socketId in this.peonPeers) {
        this.removePeer(socketId);
      }
    });

    this.wsManager.socket.on("signal", (data) => {
      this.peonPeers.get(data.socketId)?.peer.signal(data.signal);
    });
  }

  /**
   * Creates a new peer connection and sets the event listeners
   * @param {String} socketId
   *                 ID of the peer
   * @param {Boolean} iAmInitiator
   *                  Set to true if the peer initiates the connection process.
   *                  Set to false if the peer receives the connection.
   */
  addPeer(socketId: string, iAmInitiator: boolean) {
    const remotePeer = new SimplePeer({
      initiator: iAmInitiator,
      config: configuration,
    });

    remotePeer.on("connect", () => {
      this.removeOfWaiting(socketId);
    });
    remotePeer.on("signal", (data) => {
      this.wsManager.socket.emit("signal", {
        signal: data,
        socketId: socketId,
      });
    });
    remotePeer.on("data", (data) => {
      const packet = JSON.parse(data);

      log.debug("event", packet);
      this.dataListeners[packet.endpoint].forEach((callback) => {
        callback(socketId, packet);
      });
    });
    remotePeer.on("close", () => {
      remotePeer.destroy();
      this.peonPeers.delete(socketId);
    });
    this.peonPeers.set(socketId, new DataPeer(remotePeer));
  }

  on<T extends Packet>(
    endpoint: string,
    callback: (from: string, packet: T) => void
  ) {
    if (!this.dataListeners[endpoint]) {
      this.dataListeners[endpoint] = [];
    }
    this.dataListeners[endpoint].push(callback);
  }

  removePeer(socketId: string) {
    if (this.peonPeers.get(socketId))
      this.peonPeers.get(socketId).peer.destroy();
    this.peonPeers.delete(socketId);
  }
}
