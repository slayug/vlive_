import Party from "../models/Party";
import axios from "axios";
import Peon from "../models/Peon";

const BASE_URL = `https://${Config.API_HOST}`;
axios.defaults.baseURL = BASE_URL;

class Service<T> {
  constructor(private baseUrlService: string) {}

  get(url: string | number | undefined): Promise<T> {
    return axios
      .get(`${this.baseUrlService}/${url}`, { withCredentials: true })
      .then<T>((response) => response.data);
  }

  post(entity: T | object): Promise<T> {
    return axios
      .post(this.baseUrlService, entity, { withCredentials: true })
      .then<T>((response) => response.data);
  }
}

export const partyService = new Service<Party>("/party");
export const peonService = new Service<Peon>("/peon");
