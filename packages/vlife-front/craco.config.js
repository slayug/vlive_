const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@primary-color': '#1dd1a1',
              '@layout-header-background': '#ffffff',
              '@component-background': '#ffffff'
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};