HELLO WORLD!

## Start dev

Add the following line to `/etc/hosts` file:  
```
127.0.1.1  www.dev.vlive.fr dev.vlive.fr api.dev.vlive.fr
```

```bash
# SHELL 1
cd ./docker/proxy
./build.sh
cd ..
docker-compose -f docker-compose-proxy.yml up

# SHELL 2
yarn install
lerna run serve

# if you want logs from each packages
lerna run serve --stream
```
